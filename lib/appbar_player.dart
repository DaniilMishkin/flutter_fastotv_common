import 'dart:async';
import 'dart:core';

import 'package:brightness/brightness.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

enum OverlayControl { NONE, VOLUME, BRIGHTNESS, SEEK_FORWARD, SEEK_REPLAY }

class AppBarPlayer extends StatefulWidget {
  final Widget child;
  final bool absoulteBrightness;
  final bool absoulteSound;
  final Widget Function(Color? backgroundColor, Color textColor) appbar;
  final double appBarHeight;
  final Widget Function(Color? backgroundColor, Color textColor, Widget sideListButton)
  bottomControls;
  final double bottomControlsHeight;
  final void Function()? onDoubleTap;
  final void Function()? onLongTapLeft;
  final void Function()? onLongTapRight;
  final Widget Function(Color textColor)? sideList;

  const AppBarPlayer(
      {required this.child,
        required this.appbar,
        required this.bottomControls,
        this.appBarHeight = 56.0,
        required this.bottomControlsHeight,
        this.absoulteBrightness = false,
        this.absoulteSound = false,
        this.onDoubleTap,
        this.onLongTapLeft,
        this.onLongTapRight})
      : sideList = null;

  const AppBarPlayer.sideList(
      {required this.child,
        required this.appbar,
        required this.bottomControls,
        required this.sideList,
        this.appBarHeight = 56.0,
        required this.bottomControlsHeight,
        this.absoulteBrightness = false,
        this.absoulteSound = false,
        this.onDoubleTap,
        this.onLongTapLeft,
        this.onLongTapRight})
      : assert(sideList != null);

  @override
  AppBarPlayerState createState() {
    return AppBarPlayerState();
  }
}

class AppBarPlayerState extends State<AppBarPlayer>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  static const int APPBAR_TIMEOUT = 5;
  static const APPBAR_HEIGHT = 56.0;
  static const DURATION = Duration(milliseconds: 100);

  OverlayControl currentPlayerControl = OverlayControl.NONE;

  Timer? _timer;

  late AnimationController _appbarController;
  late AnimationController _bottomOverlayController;
  bool _appBarVisible = true;

  late double brightness = 0.5;

  double get overlaysOpacity => 0.5;

  final GlobalKey _gestureControllerKey = GlobalKey();

  late VolumeManager _volumeManager;

  bool isVisiblePrograms = true;
  Orientation? _orientation;

  @override
  void initState() {
    super.initState();
    _initPlatformState();
    setTimerOverlays();
    _appbarController = AnimationController(duration: DURATION, value: 1.0, vsync: this);
    _bottomOverlayController = AnimationController(duration: DURATION, value: 1.0, vsync: this);
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeMetrics() {
    if (_orientation != MediaQuery.of(context).orientation) {
      setState(() {
        _orientation = MediaQuery.of(context).orientation;
        if (_orientation != Orientation.portrait) {
          isVisiblePrograms = true;
        } else {
          isVisiblePrograms = false;
          setTimerOverlays();
        }
      });
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    allowAll();
    _appbarController.dispose();
    _bottomOverlayController.dispose();
    _timer?.cancel();
    FullscreenManager.instance.setFullscreen(true);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _orientation = MediaQuery.of(context).orientation;
    Widget child;
    if (widget.sideList == null) {
      child = _playerOverlays();
    } else {
      child = Builder(builder: (context) {
        if (_orientation == Orientation.landscape) {
          return Row(children: <Widget>[Expanded(flex: 3, child: _playerOverlays()), _sideList()]);
        }
        return Column(children: <Widget>[
          widget.appbar(backgroundColor, overlaysTextColor),
          widget.child,
          widget.bottomControls(backgroundColor, overlaysTextColor, _sideBarButton()),
          _sideList()
        ]);
      });
    }
    return Scaffold(
        backgroundColor: scaffoldColor,
        resizeToAvoidBottomInset: false,
        body: SizedBox(width: MediaQuery.of(context).size.width, child: child));
  }

  Widget _playerOverlays() {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    //Animates appBar
    final Animation<Offset> offsetAnimation = Tween<Offset>(
        begin: Offset(0.0, -(APPBAR_HEIGHT + statusBarHeight)), end: const Offset(0.0, 0.0))
        .animate(_appbarController);
    // Animates bottom
    final Animation<Offset> bottomOffsetAnimation =
    Tween<Offset>(begin: const Offset(0.0, 0.0), end: Offset(0.0, -widget.bottomControlsHeight))
        .animate(_bottomOverlayController);
    return Stack(alignment: Alignment.center, children: <Widget>[
      widget.child,

      /// Control view overlay
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _currentPlayerControlWidget(),
      ),

      /// AppBar & bottom bar
      SingleChildScrollView(
          physics: const NeverScrollableScrollPhysics(),
          child: Column(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
            AnimatedBuilder(
                animation: offsetAnimation,
                builder: (context, child) {
                  return Transform.translate(
                      offset: offsetAnimation.value,
                      child: Container(
                          color: Colors.transparent,
                          height: APPBAR_HEIGHT + statusBarHeight,
                          child: widget.appbar(backgroundColor, overlaysTextColor)));
                }),
            SizedBox(
                key: _gestureControllerKey,
                height: MediaQuery.of(context).size.height - APPBAR_HEIGHT - statusBarHeight,
                child: _gestureController),
            AnimatedBuilder(
                animation: bottomOffsetAnimation,
                builder: (context, child) {
                  return Transform.translate(
                      offset: bottomOffsetAnimation.value,
                      child: widget.bottomControls(
                          backgroundColor, overlaysTextColor, _sideBarButton()));
                })
          ]))
    ]);
  }

  Widget _sideList() {
    return !isVisiblePrograms
        ? const SizedBox()
        : Expanded(flex: 2, child: widget.sideList!(overlaysTextColor));
  }

  Widget _sideBarButton() {
    if (isPortrait(context)) {
      return const SizedBox();
    }
    return IconButton(
        icon: const Icon(Icons.list),
        color: Colors.white,
        onPressed: () {
          setState(() {
            isVisiblePrograms = !isVisiblePrograms;
          });
        });
  }

  Color? get scaffoldColor {
    if (widget.sideList == null || isLandscape(context)) {
      return Colors.black;
    }

    return null;
  }

  Color get overlaysTextColor {
    Color color;
    if (isLandscape(context)) {
      color = Colors.white;
    } else {
      color = backgroundColorBrightness(Theme.of(context).primaryColor);
    }
    return color;
  }

  Color? get backgroundColor {
    Color? color;
    if (isLandscape(context)) {
      color = Colors.black.withOpacity(overlaysOpacity);
    }
    return color;
  }

  // public:
  void setTimerOverlays() {
    _timer?.cancel();
    _timer = Timer(const Duration(seconds: APPBAR_TIMEOUT), () {
      if (isLandscape(context)) {
        setState(() {
          setOverlaysVisible(false);
        });
      } else {
        _timer?.cancel();
      }
    });
  }

  void setOverlaysVisible(bool visible) {
    _appBarVisible = visible;

    if (_appBarVisible == true) {
      _appbarController.forward();
      _bottomOverlayController.forward();
      FullscreenManager.instance.setFullscreen(true);
      setTimerOverlays();
    } else {
      if (_timer!.isActive) {
        _timer!.cancel();
      }
      if(!kIsWeb){
        FullscreenManager.instance.setFullscreen(false);
      }
      _appbarController.reverse();
      _bottomOverlayController.reverse();
    }
  }

  void _initPlatformState() async {
    if (!kIsWeb) {
      _volumeManager = await VolumeManager.getInstance();
      ScreenBrightness.value.then((bright) {
        setState(() {
          brightness = bright ?? 0.5;
        });
      });
    }
  }

  Widget get _gestureController {
    return Center(
        child: GestureDetector(
            onTap: () {
              _appBarVisible ? setOverlaysVisible(false) : setOverlaysVisible(true);
            },
            onVerticalDragUpdate: (DragUpdateDetails details) {
              if (!kIsWeb) {
                final isLeftPart = details.localPosition.dx < MediaQuery.of(context).size.width / 2;
                isLeftPart
                    ? _onVerticalDragUpdateVolume(details)
                    : _onVerticalDragUpdateBrightness(details);
              }
            },
            onVerticalDragStart: (DragStartDetails details) {
              if (!kIsWeb) {
                _handleVerticalDragStart(details);
              }
            },
            onVerticalDragEnd: (DragEndDetails details) {
              if (!kIsWeb) {
                _handleVerticalDragEnd(details);
              }
            },
            onLongPressStart: (LongPressStartDetails details) {
              final isLeftPart = details.localPosition.dx < MediaQuery.of(context).size.width / 2;
              isLeftPart ? widget.onLongTapLeft?.call() : widget.onLongTapRight?.call();
            },
            onDoubleTap: () {
              if (widget.onDoubleTap != null) {
                widget.onDoubleTap!();
                if (isLandscape(context)) {
                  currentPlayerControl = OverlayControl.NONE;
                }
              }
            },
            child: Container(color: Colors.transparent)));
  }

  void _onVerticalDragUpdateVolume(DragUpdateDetails details) {
    final maxHeight = MediaQuery.of(context).size.height;
    final maxVol = _volumeManager.maxVolume();
    double currentVol = _volumeManager.currentVolume();
    if (details.localPosition.dy > 0 && details.localPosition.dy <= maxHeight) {
      setState(() {
        if (widget.absoulteSound) {
          currentVol = (maxHeight - details.localPosition.dy) * maxVol / maxHeight;
        } else {
          final oneStep = maxVol / maxHeight;
          currentVol -= 1 * oneStep * details.delta.dy;
          currentVol = currentVol.clamp(0, maxVol);
        }
      });
    }

    _volumeManager.setVolume(currentVol);
  }

  void _onVerticalDragUpdateBrightness(DragUpdateDetails details) {
    final maxHeight = MediaQuery.of(context).size.height;
    if (details.localPosition.dy > 0 && details.localPosition.dy <= maxHeight) {
      setState(() {
        if (widget.absoulteBrightness) {
          brightness = (maxHeight - details.localPosition.dy) / maxHeight;
        } else {
          final oneStep = 1 / maxHeight;
          brightness -= oneStep * details.delta.dy;
          brightness = brightness.clamp(0, 1.0);
        }
      });
    }
    ScreenBrightness.set(brightness);
  }

  /// Makes control widget invisible and resets current control
  void _handleVerticalDragEnd(DragEndDetails details) {
    setState(() {
      currentPlayerControl = OverlayControl.NONE;
    });
  }

  /// Sets current control [brightness] or [volume] widget and makes it visible
  void _handleVerticalDragStart(DragStartDetails details) {
    setState(() {
      final isLeftPart = details.localPosition.dx < MediaQuery.of(context).size.width / 2;
      currentPlayerControl = isLeftPart ? OverlayControl.VOLUME : OverlayControl.BRIGHTNESS;
    });
  }

  List<Widget> _currentPlayerControlWidget() {
    if (currentPlayerControl == OverlayControl.VOLUME) {
      final maxVol = _volumeManager.maxVolume();
      final currentVol = _volumeManager.currentVolume();
      return [
        const Icon(Icons.volume_up, color: Colors.white, size: 84),
        Text((currentVol / maxVol * 100).toStringAsFixed(0) + "%",
            style: const TextStyle(fontSize: 84, color: Colors.white))
      ];
    } else if (currentPlayerControl == OverlayControl.BRIGHTNESS) {
      return [
        const Icon(Icons.brightness_high, color: Colors.white, size: 84),
        Text((brightness * 100).toStringAsFixed(0) + "%",
            style: const TextStyle(fontSize: 84, color: Colors.white))
      ];
    } else if (currentPlayerControl == OverlayControl.SEEK_REPLAY) {
      return [const Icon(Icons.replay_5, color: Colors.white, size: 84)];
    } else if (currentPlayerControl == OverlayControl.SEEK_FORWARD) {
      return [const Icon(Icons.forward_5, color: Colors.white, size: 84)];
    }
    return [const SizedBox()];
  }
}
